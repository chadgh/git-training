#!/usr/bin/env python
"""Tests something that should not need testing."""


def test():
    """Simple test."""
    assert 1 + 2 == 2


def test2():
    """Slightly more complex test."""
    assert 100 / 10 == 10


if __name__ == '__main__':
    test()
    test2()
